from django.contrib import admin
from .models import Account, Receipt, ExpenseCategory


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    pass
    # list_display = (
    #     "name",
    #     "number",
    #     "owner",
    #     "id",
    # )


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    pass
    # list_display = (
    #     "vendor",
    #     "total",
    #     "tax",
    #     "date",
    #     "purchaser",
    #     "category",
    #     "account",
    #     "id",
    # )


@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    pass
    # list_display = (
    #     "name",
    #     "owner",
    #     "id",
    # )
